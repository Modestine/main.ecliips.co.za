<?php
require_once("includes/headerindex.php");
?>
<!-- sliders -->
<div id="sliders">
  <div class="full-width">
    <!-- light slider -->
    <div id="light-slider" class="carousel slide">
      <div id="carousel-area">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <!-- <li data-target="#carousel-slider" data-slide-to="0" class="active"></li> -->
            <!-- <li data-target="#carousel-slider" data-slide-to="1"></li> -->
            <!--  <li data-target="#carousel-slider" data-slide-to="2"></li> -->
          </ol>
          <!-- <div class="carousel-inner" role="listbox"> -->
          <div>
            <div class="carousel-item active ">
              <img src="img/about/aboutimg.png" alt="">
              <div class="carousel-caption pr-4">
                <!-- <a href="#specials" class="btn btn-lg btn-border animated fadeInUp">JOIN US</a> -->

              </div>
            </div>

          </div>
          <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
          </a>
          <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
            <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End sliders -->

</header>

<!-- About Section Start -->
<div id="" class="section-padding ecliipsBB">
  <div class="container">

    <div class="row text-center">

      <div class="col-md-3">
      </div>

      <div class="col-md-2">
        <img class="rounded-circle" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-2">
        <img class="rounded-circle z-depth-2" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-2">
        <img class="rounded-circle z-depth-2" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-3">
      </div>

    </div>

    
    <div class="row text-center">

      <div class="col-md-3">
      </div>

      <div class="col-md-2">
        <img class="rounded-circle" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-2">
        <img class="rounded-circle z-depth-2" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-2">
        <img class="rounded-circle z-depth-2" alt="100x100" src="img/team/Axel.png" data-holder-rendered="true">
        <p class="text-white p-3">
          Axel Siphon <br>
          Lorem Ipsum
        </p>
      </div>

      <div class="col-md-3">
      </div>

    </div>

  </div>

</div>
<hr style="height:2px;border-width:0;color:gray;background-color:gray">

<?php
require_once("includes/footer.php");
?>

<script>
document.addEventListener('DOMContentLoaded', changeColor);

function changeColor(){
    var element = document.getElementById("navId");
  element.classList.add("navgrey");
}
</script>