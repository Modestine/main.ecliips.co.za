<?php require_once("includes/header.php"); ?>
<?php require_once("includes/classes/UploadHandler.php"); ?>
<?php

if (!isset($_COOKIE["QueenandHairCookie"])) {
  header("Location: signin.php");
}
?>

<div class="container">
  <div class="row pt-5 mt-5">
    <div class="col-md-12">
      <h5 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">New Article</h5>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <form action='processing.php' method='POST' enctype='multipart/form-data'>
        <div class="form-group">
          <label for="exampleInputEmail1">Title</label>
          <input type="text" class="form-control" id="title" name="title" required>

        </div>

        <div class="form-group">
          <label for="description">Content</label>
          <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
        </div>

        <div class="form-group">
          <label for="file1">Choose the image of the article</label>
          <input type="file" class="form-control-file" id="file1" name="file1" required>
        </div>

        <input type="submit" name="uploadButton" id="uploadButton" class="btn btn-primary" value="Publish" />
      </form>
    </div>
  </div>
</div>



<div id="loading" class="modal">
  <div class='row'>
    <div class='col s12'>
      <div class="modal-content">
        <h4 class="center-align">Wait...</h4>
        <h5 class="center-align">
          <br>
          <br>
          <img src="img/loading-spinner.gif" alt="patientez">
        </h5>

      </div>
    </div>
  </div>
</div>

<?php require_once("includes/footer.php"); ?>