<?php
require_once("includes/headerindex.php");
?>
<!-- sliders -->
<div id="sliders">
  <div class="full-width">
    <!-- light slider -->
    <div id="light-slider" class="carousel slide">
      <div id="carousel-area">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <!-- <li data-target="#carousel-slider" data-slide-to="0" class="active"></li> -->
            <!-- <li data-target="#carousel-slider" data-slide-to="1"></li> -->
            <!--  <li data-target="#carousel-slider" data-slide-to="2"></li> -->
          </ol>
          <!-- <div class="carousel-inner" role="listbox"> -->
          <div>
            <div class="carousel-item active ">
              <img src="img/contact/contactimg2.png" alt="">
              <div class="carousel-caption pr-4">
                <a href="#contact" class="btn btn-lg btn-border animated fadeInUp">CONTACT US</a>

              </div>

            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- End sliders -->
<!-- <img src="img/contact/pattern.png" alt="" width="100"> -->
</header>



<!-- Contact Form Section Start -->
<section id="contact" class="contact-form section-padding pt-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title wow fadeInDown animated text-white" data-wow-delay="0.3s">Contact Us</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-6 col-xs-12">
                <h3 class="title-head text-left text-white">Get in touch</h3>
                <?php
                if (isset($_GET['emailSent'])) {
                ?>
                    <p class="bg-success">Thank you for contacting us, we will come back to you soon.</p>
                <?php
                }
                ?>

                <?php
                if (isset($_GET['emailNotSent'])) {
                ?>
                    <p class="bg-danger">Sorry your email has not been sent.</p>
                <?php
                }
                ?>

                <form class="contact-form" data-toggle="validator" action="processing.php" method="POST">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-xs-12">
                            <div class="form-group">
                                <i class="contact-icon fa fa-user"></i>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required data-error="Please enter your name">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-xs-12">
                            <div class="form-group">
                                <i class="contact-icon fa fa-envelope-o"></i>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required data-error="Please enter your email">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-xs-12">
                            <div class="form-group">
                                <i class="contact-icon fa fa-pencil-square-o"></i>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required data-error="Please enter your Subject">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message" required data-error="Please enter your message"></textarea>
                            <div class="help-block with-errors"></div>
                            <input type="submit" id="sendEmail" name="sendEmail" class="btn btn-common btn-form-submit" value="Send Message"></input>
                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12">
                <h4 class="contact-info-title text-left text-white">Contact Information</h4>
                <div class="contact-info ">
                    <address class="text-white">
                        <i class="lni-map-marker icons cyan-color contact-info-icon text-white"></i>
                        Level 13, 2 Hope St, Capetown,
                    </address>
                    <div class="tel-info">
                        <a href="tel:1800452308" class="text-white"><i class="lni-mobile icons cyan-color contact-info-icon text-white"></i>+27 81 533 12 48</a>
                    </div>
                    <a href="mailto:hello@spiritapp.com" class="text-white"><i class="lni-envelope icons cyan-color contact-info-icon text-white"></i>info@ecliips.co.za</a>
                    <a href="#" class="text-white"><i class="lni-tab icons cyan-color contact-info-icon text-white"></i>www.ecliips.co.za</a>
                    <!-- <ul class="social-links">
                        <li>
                            <a href="#" class="fa fa-facebook"></a>
                        </li>
                        <li>
                            <a href="#" class="fa fa-twitter"></a>
                        </li>
                        <li>
                            <a href="#" class="fa fa-instagram"></a>
                        </li>
                        <li>
                            <a href="#" class="fa fa-linkedin"></a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Form Section End -->
<hr style="height:2px;border-width:0;color:gray;background-color:gray">

<?php
require_once("includes/footer.php");
?>

