<?php
require_once("includes/header.php");
require_once("includes/classes/ArticlesHandler.php");
$ArticlesHandler = new ArticlesHandler($con);
$properties = $ArticlesHandler->getProperties();
$property1 = new Property($con, $properties, null);
?>
<?php

if (!isset($_COOKIE["QueenandHairCookie"])) {
  header("Location: signin.php");
}
?>

<!-- Team Section Start -->

<div id="team" class="team-members-tow section-padding">
  <div class="container pt-5 ">
    <div class="row">
      <div class="col-md-12">
        <h2 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Admin Blog</h2>

      </div>
    </div>
    <p> <a href="publish.php" class="btn btn-dark mr-2">New Article</a> <a href="blog.php" class="btn btn-dark mr-2">Go to Blog</a> <a href="index.php" class="btn btn-dark mr-2">Home</a> <a href="signout.php" class="btn btn-danger mr-2">Sign Out</a></p>

    <?php

    require_once("includes/classes/ArticlesHandler.php");


    $ArticlesHandler = new ArticlesHandler($con);
    $properties = $ArticlesHandler->getProperties();


    $property1 = new Property($con, $properties, null);

    ?>
    <div class="row">
      <div class="col-sm-12">
        <?php

        if (sizeof($properties) > 0) {

          echo $property1->generateItemsFromMyproperties($properties);
        } else {
          echo '<div class="text-center">
                  <div class="alert alert-info" role="alert">
                      No article publish
                  </div>
                </div>';
        }

        ?>

      </div>
    </div>
  </div>
</div>
<!-- Team Section End -->


<?php
require_once("includes/footer.php");
?>