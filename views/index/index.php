

<section>
    <div class="container">
        <h1>Email Plugin</h1>
        <div class="card">
            <div class="card-body">
                <a href="<?php pluginUrl('email','index/install'); ?>">Install</a>
            </div>
        </div>
        <h1>Email</h1>

        <a href="<?php pluginUrl('email','index/sendemail'); ?>">Send Basic Email</a>
        <br>
        <a href="<?php pluginUrl('email','index/sendemailwithtemplate'); ?>">Send Email with Template</a>
        <br>
        <a href="<?php pluginUrl('email','index/SendEmailTemplate'); ?>">Send Email With DB Template</a>

    </div>
</section>
