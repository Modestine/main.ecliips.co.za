<?php
require_once("includes/header.php");
require_once("includes/classes/Constants.php");
require_once("includes/classes/FormSanitizer.php");

  if (isset($_COOKIE["QueenandHairCookie"])) {
  header("Location: home.php");
}

$account = new Account($con);

if (isset($_POST["submitButton"])) {

  $phone_number = FormSanitizer::sanitizeFormNumber($_POST["phone_number"]);
  $password = FormSanitizer::sanitizeFormPassword($_POST["password"]);

  $result = $account->login($phone_number, $password);
  if ($result) {

    $_SESSION["userLoggedIn"] = $phone_number;
    setcookie("QueenandHairCookie", $phone_number, time() + 60*60*365, "/");  
    header("Location: product.php");
  }
};

function getInputValue($name)
{
  if (isset($_POST[$name])) {
    echo $_POST[$name];
  }
}
?>

<style>
  input {
    font-size: 1.3rem !important;
  }
</style>


<div class="container ">
<div class="row pt-5 mt-5">
          <div class="col-md-12">
            <h5 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Admin Login</h5>
          </div>
        </div>

<div class="card pt-5 mt-5" >
  <div class="card-body">
  <form action="signin.php" method="POST" class="was-validated">
  <div class="form-group">

    <?php echo $account->getError(Constants::$loginFailed); ?>
              <label for="phone_number">Numéro de téléphone</label>
              <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php getInputValue('phone_number') ?>" maxlength="10" required>
              
  </div>
  <div class="form-group">
  <label for="password">Mot de passe</label>
              <input type="password" class="form-control" id="password" name="password" autocomplete="off" required aria-describedby="pass">
              <small id="pass" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <input type="submit" name="submitButton" id="submitButton" class="btn btn-primary" value="Connexion">
</form>
  </div>
</div>
</div>


<?php require_once("includes/footer.php"); ?>