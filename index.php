<?php
require_once("includes/headerindex.php");
?>
<!-- sliders -->
<div id="sliders">
  <div class="full-width">
    <!-- light slider -->
    <div id="light-slider" class="carousel slide">
      <div id="carousel-area">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <!-- <li data-target="#carousel-slider" data-slide-to="0" class="active"></li> -->
            <!-- <li data-target="#carousel-slider" data-slide-to="1"></li> -->
            <!--  <li data-target="#carousel-slider" data-slide-to="2"></li> -->
          </ol>
          <!-- <div class="carousel-inner" role="listbox"> -->
          <div>
            <div class="carousel-item active ">
              <img src="img/slider/home1.png" alt="">
              <div class="carousel-caption pr-4">
                <!-- <h3 class="slide-title animated fadeInDown ">Welcome to <span>Ecliips</span> Online Store​.</h3>
                <p class="text-white animated fadeIn">Make Your Dream Alive​.</p>
                <p class="text-white">Adaptability - Creativity - Consistency </p> -->
                <a href="#specials" class="btn btn-lg btn-border animated fadeInUp">SHOP NOW</a>
                <!-- <a href="#stores" class="btn btn-lg btn-danger animated fadeInUp">Our stores</a> -->

              </div>
            </div>

            <!-- <div class="carousel-item">
              <img src="img/slider/home2.png" alt=""> -->
            <!-- <div class="carousel-caption pt-4">
                <h3 class="slide-title animated fadeInDown">Download our apps</h3>
                <h5 class="slide-text animated fadeIn">Download the to get the all experience out of our business.</h5>
                <a href="#" class="animated fadeInUp" target="_blank"><img src="https://don16obqbay2c.cloudfront.net/wp-content/themes/ecwid/images/badges/gray-app-store.svg" alt="Available on the App Store"  id="applestore" ></a>
                <a href="#" class="animated fadeInUp" target="_blank"><img src="https://don16obqbay2c.cloudfront.net/wp-content/themes/ecwid/images/badges/gray-google.svg" alt="Get it on Google play"  id="googlestore" ></a>
              </div> -->
            <!-- </div> -->


            <!-- <div class="carousel-item">
              <img src="img/slider/logobackground.jpg" alt="">
              <div class="carousel-caption">
                <h3 class="slide-title animated fadeInDown"><span>100+</span> UI Blocks & Components</h3>
                <h5 class="slide-text animated fadeIn">Lorem ipsum dolor sit amet, consectetuer adipiscing elit<br> Curabitur ultricies nisi Nam eget dui. Etiam rhoncus</h5>
                <a href="#" class="btn btn-lg btn-border animated fadeInUp">Get Started</a>
                <a href="#" class="btn btn-lg  animated fadeInUp">Download</a> 
              </div>
            </div>-->
          </div>
          <!-- <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
          </a>
          <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
            <i class="fa fa-chevron-right"></i>
          </a> -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End sliders -->

</header>
<!-- Header Area wrapper End -->
<div id="" class="" style="background-color: black;">
  <div class="container">
    <div class="row section-padding">
      <div class="col-sm-6 p-5">
        <img src="img/home/ecliipsplanet.png" alt="" width="100">

      </div>
      <div class="col-sm-4 pt-3 mt-3 p-5">
        <p class="text-left text-white">ABOUT US</p>
        <p class="text-left text-white">
          Ecliips is an african tech-based Project Management Firm that offers effective assistance in delivering entrepreneurs, individuals, businesses and NGOs projects successfully.
        </p>
        <a href="about.php" class="btn btn-lg btn-border animated fadeInUp">LEARN MORE</a>
      </div>
    </div>
  </div>
</div>

<!-- About Section Start -->
<div id="" class="section-padding ecliipsBB">
  <div class="container">
    <div class="row" style="background: url('img/home/stars.png');">
      <div class="col-md-12">
        <h4 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Our Stores</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <img src="img/home/foodplatter.png" alt="">
          <h5 class="pt-4"><a class="textStore" href="#">Food</a></h5>
        </div>

      </div>

      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <img src="img/home/woman.png" alt="">
        </div>
        <h5 class="pt-4"><a class="textStore"  href="#">Health Care</a></h5>
      </div>

      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <img src="img/home/woman1.png" alt="">
        </div>
        <h5 class="pt-4"><a class="textStore"  href="#">Afro Fusion Boutique</a></h5>
      </div>

    </div>
  </div>

</div>
</div>

<div id="" class="ecliipsYB">
  <div class="container">
    <!-- <div class="row">
      <div class="col-md-12">
        <h5 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Giveback</h5>
      </div>
    </div> -->
    <div class="row">
      <div class="col-md-12">
        <img src="img/home/accents.png" alt="Snow" >
        <div class="textAccents"> <strong>Join our CSR programs and take this Opportunity to engage in the education of our communities that are still illiterate, fund the development of our infrastructures and participate in the conservation of our precious ecosystem. </strong></div>
      </div>

    </div>
  </div>
</div>





<?php
require_once("includes/footer.php");
?>