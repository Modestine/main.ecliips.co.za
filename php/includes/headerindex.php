<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Ecliips Online Store</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
    <!-- Font -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="css/slicknav.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Extras Style -->
    <link rel="stylesheet" type="text/css" href="css/extras.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a href="index.php" class="navbar-brand"><img src="img/logo2.jpeg" alt="" ></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix ">
              <li class="nav-item">
                <a class="nav-link" href="index.php">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="estores.php">
                  E-stores
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php"> 
                  About-us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">
                Contact-us
                </a>
              </li>

              <!-- <li>
              <a class="nav-link" href="#">
              <i class="fa fa-shopping-cart"></i>
              </li> -->
              <!-- <li class="nav-item"> -->
                <!-- <a class="nav-link" href="more.php"> -->
                <!-- <a class="nav-link" href="#">
                  More
                </a>
              </li> -->


            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu navbar-nav">
          <li>
            <a class="page-scroll" href="index.php">
              Home
            </a>
          </li>
          <li>
            <a class="page-scroll" href="estores.php">
            E-stores
            </a>
          </li>
          <li>
            <a class="page-scroll" href="about.php">
            About-us
            </a>
          </li>
          <li>
            <a class="page-scroll" href="contact.php">
            Contact-us
            </a>
          </li>
          <!-- <li> -->
            <!-- <a class="page-scroll" href="more.php"> -->
            <!-- <a class="nav-link" href="#">
            More
            </a>
          </li> -->
                  
        <!-- <li>
              <a class="nav-link" href="#">
              <i class="fa fa-shopping-cart"></i>
              </li>-->
        </ul> 
        <!-- Mobile Menu End -->


      </nav>
      <!-- Navbar End -->