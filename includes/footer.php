
	  <!-- Footer Section -->
    <footer class="footer">
      <!-- Container Starts -->
      <div class="container">
        <!-- Row Starts -->
        <div class="row section">
          <!-- Footer Widget Starts -->
          <div class="footer-widget col-lg-6 col-md-12 col-xs-12 wow fadeIn">
            <h3 class="small-title">
              About Us
            </h3>
            <p>
            Delivering Business and NGO projects since 2014,
            Ecliips is a Project Management Firm that offers 
            a wide variety of Quality-Oriented Products and Services 
            by Africa for Africa, and everyone who loves Africa.
            </p>
            <div class="social-footer">
            <a href="#" class="animated fadeInUp" data-toggle="modal" data-target="#exampleModalCenter"><img src="https://don16obqbay2c.cloudfront.net/wp-content/themes/ecwid/images/badges/gray-app-store.svg" alt="Available on the App Store"  id="applestorefooter" ></a>
                <a href="#" class="animated fadeInUp" data-toggle="modal" data-target="#exampleModalCenter"><img src="https://don16obqbay2c.cloudfront.net/wp-content/themes/ecwid/images/badges/gray-google.svg" alt="Get it on Google play"  id="googlestorefooter" ></a>
            </div>
          </div>
          <!-- Footer Widget Ends -->
           
          <!-- Footer Widget Starts -->
          <div class="footer-widget col-lg-3 col-md-6 col-xs-12 wow fadeIn" data-wow-delay=".2s">
            <h3 class="small-title">
              Links
            </h3>
            <ul class="menu">
              <li><a href="index.php">Home</a></li>
              <!-- <li><a href="estores.php">E-stores</a></li> -->
              <li><a href="about.php">About-us</a></li>
              <li><a href="Contact.php">Contact-us</a></li>
              <li><a href="#">More</a></li>
            </ul>
          </div>

          <!-- Footer Widget Ends -->

          <div class="footer-widget col-lg-3 col-md-6 col-xs-12 wow fadeIn" data-wow-delay=".2s">
            <h3 class="small-title">
              Social Media
            </h3>
            <ul class="menu">
            <li> <a href="https://www.instagram.com/ecliips.projects/"><i class="fa fa-instagram icon-round pr-2" style="color: purple;"></i>Instagram</a></li>
              <li> <a href="#"><i class="fa fa-twitter icon-round pr-2 text-info "></i>Twitter</a></li>
              <!-- <li> <a href="#"><i class="fa fa-youtube icon-round pr-2"></i>youtube</a></li> -->
              <li> <a href="#"><i class="fa fa-facebook icon-round pr-2" style="color:dodgerblue"></i>Facebook</a></li>


              <li> <a href="#"><i class="fa fa-google icon-round pr-2 text-warning"></i>Google</a></li>
            </ul>
          </div>
          <!-- Footer Widget Ends -->


          <!-- Footer Widget Starts -->
          <!-- <div class="footer-widget col-lg-3 col-md-6 col-xs-12 wow fadeIn" data-wow-delay=".8s">
            <h3 class="small-title">
              SUBSCRIBE US
            </h3>
            <div class="contact-us">
              <form>
                <div class="form-group">
                  <input type="text" class="form-control" id="exampleInputName2" placeholder="Enter your name">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter your email">
                </div>
                <button type="submit" class="btn btn-common">Submit</button>
              </form>
            </div>
          </div> -->
          <!-- Footer Widget Ends -->
        </div>
        <!-- Row Ends -->
      </div>
      <!-- Container Ends -->

      <!-- Copyright -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
            <p class="copyright-text">All copyrights reserved © Ecliips 2020 - Designed &amp; Developed by <a rel="nofollow" href="http://modestinewebservices.com/">Modestine Web Services</a>
              </p>
            </div>
            <!-- <div class="col-lg-6 col-md-6 col-xs-12">
              <ul class="nav nav-inline  justify-content-end ">
                <li class="nav-item">
                  <a class="nav-link active" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Sitemap</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Privacy Policy</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Terms of services</a>
                </li>
              </ul>
            </div> -->
          </div>
        </div>
      </div>
      <!-- Copyright  End-->

    </footer>
    <!-- Footer Section End-->
    <!-- Go to Top Link -->
<a href="#" class="back-to-top">
  <i class="fa fa-arrow-up"></i>
</a>

<!-- Preloader -->
<div id="preloader">
  <div class="loader" id="loader-1"></div>
</div>

<!-- End Preloader -->
<!-- start model -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Ecliips Online Store Apps.</h5>
      </div>
      <div class="modal-body">
      Ecliips new apps will be available on Android and iOS. Come back and check us later.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary bg-dark" data-dismiss="modal">Okay</button>
      </div>
    </div>
  </div>
</div>
<!-- end model -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/scrolling-nav.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/form-validator.min.js"></script>
<script src="js/contact-form-script.js"></script>
<script src="js/main.js"></script>

</body>

</html>