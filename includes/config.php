<?php

ob_start(); //Turns on output buffering 
session_start();

date_default_timezone_set("Africa/Harare");
define('SITE_ROOT', __DIR__);

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080") {
    try {

        $con = new PDO("mysql:dbname=ecliipsdb;host=localhost", "root", "");
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
} else {
    try {

        $con = new PDO("mysql:dbname=ecliipsdb;host=localhost", "ecliips", "Ecliips@1502");

        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage() . "no connection oops";
    }
}
