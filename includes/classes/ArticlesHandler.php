<?php

class ArticlesHandler {

    private $con;

    public function __construct($con){
        $this->con = $con;
    }

    public function getProperties() {
        if (isset($_GET["products"])) {
            $numberProducts = $_GET["products"];
            $limit = (int)$numberProducts;
            $queryAdd = 'LIMIT ' . $limit;
        }else{
            $queryAdd = "";
        }

        if (isset($_GET["category"])) {
            $categoryNumber = $_GET["category"];
            $categoryInt = (int)$categoryNumber;
            $queryAdd2 = 'WHERE category_id = ' . $categoryInt;
        }else{
            $queryAdd2 = "";
        }
        $query = $this->con->prepare("   
        SELECT `products`.`id`,
        `products`.`userId`,
        `products`.`type`,
        `products`.`price`,
        `products`.`reduced_price`,
        `products`.`uploadedBy`,
        `products`.`title`,
        `products`.`description`,
        `products`.`takealotLink`,
        `products`.`filePath1`,
        `products`.`filePath2`,
        `products`.`filePath3`,
        `products`.`category`,
        `products`.`uploadDate`,
        `products`.`views`,
        `products`.`category_id`,
        `categories`.`value`,
        `categories`.`name`
    FROM `products`
     LEFT JOIN categories ON categories.id = products.category_id $queryAdd2 ORDER BY uploadDate DESC $queryAdd ") ;
  
        $query->execute();

        $properties = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $property = new Property($this->con, $row, null);
            array_push($properties, $property);
        }

        return $properties;

    }


    public function getPropertiesPage2() {
        $query = $this->con->prepare("      SELECT 
        `properties`.`id`,
        `properties`.`userId`,
        `properties`.`type`,
        `properties`.`price`,
        prt.name,
        `properties`.`uploadedBy`,
        `properties`.`title`,
        `properties`.`description`,
        `properties`.`filePath1`,
        `properties`.`filePath2`,
        `properties`.`filePath3`,
        `properties`.`category`,
        `properties`.`uploadDate`,
        `properties`.`views`,
        `properties`.`property_type_id`
         FROM properties LEFT JOIN property_type prt ON prt.id = properties.property_type_id ORDER BY uploadDate DESC LIMIT 25 OFFSET 25") ;
        $query->execute();

        $properties = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $property = new Property($this->con, $row, null);
            array_push($properties, $property);
        }

        return $properties;

    }

    public function getPropertiesPage3() {
        $query = $this->con->prepare("      SELECT 
        `properties`.`id`,
        `properties`.`userId`,
        `properties`.`type`,
        `properties`.`price`,
        prt.name,
        `properties`.`uploadedBy`,
        `properties`.`title`,
        `properties`.`description`,
        `properties`.`filePath1`,
        `properties`.`filePath2`,
        `properties`.`filePath3`,
        `properties`.`category`,
        `properties`.`uploadDate`,
        `properties`.`views`,
        `properties`.`property_type_id`
         FROM properties LEFT JOIN property_type prt ON prt.id = properties.property_type_id ORDER BY uploadDate DESC LIMIT 25 OFFSET 50") ;
        $query->execute();

        $properties = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $property = new Property($this->con, $row, null);
            array_push($properties, $property);
        }

        return $properties;

    }
}

?>