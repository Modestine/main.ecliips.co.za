
<?php
class UploadHandler
{

  private $con;

  public function __construct($con)
  {
    $this->con = $con;
  }

  public function createUploadForm()
  {

    $title = $this->createTitle();

    $decription = $this->createDescription();
    $file = $this->createFile();
    $uploadButton = $this->createUploadButton();
    $type = $this->createType();
    $price = $this->createPrice();
    $reducedprice = $this->reducedPrice();


    return "<form action='processing.php' method='POST' enctype='multipart/form-data'>
    <div class='form-row align-items-center'>
    <div class='col-auto'>
      <div class='card'>
        <div class='card-body'>

        <p>Les champs avec astérique sont obligatoires. </p>
                $title
                $type
                $price
                $reducedprice
                $decription
                $file
                $uploadButton
                </div>
                </div>
              </div>
            </div>
              </div>  
        </form>";
  }

  private function createTitle()
  {
    return "
        <div class='form-group ' >
        <label for='title'>Name *</label>
          <input id='title' type='text'  name='title' class='form-control' required>

        </div>
    ";
  }

  private function createPrice()
  {
    return "<div class='input-field ' >
          <input id='price' type='number' data-length='25' name='price'>
          <label for='price'>Price</label>
        </div>";
  }



  private function reducedPrice()
  {
    return "
        <div class='form-group ' >
        <label for='title'>Reduced Price *</label>
        <div class='input-group-prepend'>
        <span class='input-group-text' id='validatedInputGroupPrepend'>@</span>
      </div>
          <input id='reduced_price' type='number'  name='reduced_price' class='form-control' required>

        </div>
    ";
  }

  private function createDescription()
  {

    return " <div class='form-group ' >
    <label for='description'>Description *</label>
                  <textarea id='description' name='description' class='form-control' data-length='120' required></textarea>
    
                </div>
            ";
  }

  private function createFile()
  {
    return "
    <div class='custom-file'>
    <input type='file' name='file1' class='custom-file-input' id='customFile' required>
    <label class='custom-file-label' for='file1'>Choose photo *</label>
  </div>

";
  }

  private function createUploadButton()
  {
    return "  <div class='center-align'> <input type='submit' name='uploadButton' id='uploadButton' class='btn teal waves-effect waves-light ' value='Publier'></div>";
  }

  private function createType()
  {
    return " 
    <div class='form-group'>
    <label  for='type'>Type *</label>
    <select class='form-control' id='type' name='type'>
      <option value='1'>Wig</option>
      <option value='2'>Weave</option>

    </select>
    </div>";
  }

  private function createVille()
  {
    return " 
    <label  for='type'>Ville (choisissez votre ville) *</label>
    <select name='ville'>
      <option value='1'>Lubumbashi</option>
      <option value='2'>Kinshasa</option>
      <option value='3'>Mbuji-Mayi</option>
      <option value='4'>Kananga</option>
      <option value='5'>Kisangani</option>
      <option value='6'>Bukavu</option>
      <option value='7'>Tshikapa</option>
      <option value='8'>Kolwezi</option>
      <option value='9'>Likasi</option>
      <option value='10'>Goma</option>
      <option value='11'>Kipushi</option>
    </select>";
  }

  private function createNumberOfRooms()
  {
    return "
    <label for='numberOfRooms'>Nombre de chambres</label>
    <select name='numberOfRooms'>
      <option value='1'>1</option>
      <option value='2'>2</option>
      <option value='3'>3</option>
      <option value='4'>4</option>
      <option value='5'>5</option>
      <option value='6'>6</option>
      <option value='7'>7</option>
      <option value='8'>8</option>
      <option value='9'>9</option>
      <option value='10'>10</option>
      <option value='11'>11</option>
      <option value='12'>12</option>
      <option value='13'>13</option>
      <option value='14'>14</option>
      <option value='15'>15</option>
    </select>";
  }
}

?>