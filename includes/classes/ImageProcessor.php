<?php



class ImageProcessor {

    private $con;
    private $sizeLimit = 5242880;
    private $allowedTypes = array("png", "jpg", "jpeg", "");

    public function __construct($con) {
        $this->con = $con;
    }

    public function upload($ImageUploadData) {
        $targetDir = "uploads/images/";
        $image1 = $ImageUploadData->file1; 
        $image2 = $ImageUploadData->file2;
        $image3 = $ImageUploadData->file3;

        $images = array($image1, $image2, $image3);

        $arrayPath = array();
        foreach ($images as $image) {
            $tempFilePath = $targetDir . uniqid() . basename($image["name"]);
            $tempFilePath = str_replace(" ", "_", $tempFilePath);
    
            $isValideData = $this->processData($image, $tempFilePath);
    
            if(!$isValideData){
                return false;
            }
           
            if(move_uploaded_file($image["tmp_name"], $tempFilePath)){
                array_push($arrayPath, $tempFilePath);
            }
        }
               
            if(!$this->insertImageData($ImageUploadData, $arrayPath)){
                echo "Insert query failed$";
                return false;
            }

            return true;
    }

    private function processData($image, $filePath) {

        $imageType = pathinfo($filePath, PATHINFO_EXTENSION);

        if(!$this->isValidType($imageType)){
            echo "Format de l'image pas supporté, l'image doit etre en .png, ou .jpg";
            return false;

        }elseif (!$this->isValidSize($image)) {
            $size = $this->sizeLimit/1048576;
            echo "L'image a une taille superièure à " . $size. " MBytes";
            return false;

        }elseif($this->hasError($image)){
            echo "Error Code: " . $image["error"];
        }
        return true;
    }

    private function isValidType($type) {
        $lowercase = strtolower($type); 
        return in_array($lowercase, $this->allowedTypes);
    }

    private function isValidSize($data) {
        return $data["size"] <= $this->sizeLimit;
    }

    private function hasError($data) {
        return $data["error"] !=0;
    }

    private function insertImageData($ImageUploadData, $arrayPath) {
        $query = $this->con->prepare("INSERT INTO products(title, userId, uploadedBy, description, filePath1, filePath2, filePath3)
        VALUES(:title, :userId, :uploadedBy, :description, :filePath1, :filePath2, :filePath3) ");

        $query->bindParam(":title", $ImageUploadData->title);
        $query->bindParam(":userId", $ImageUploadData->userId);
        $query->bindParam(":uploadedBy", $ImageUploadData->uploadedBy);
        $query->bindParam(":description", $ImageUploadData->description);
        $query->bindParam(":filePath1", $arrayPath[0]);
        $query->bindParam(":filePath2", $arrayPath[1]);
        $query->bindParam(":filePath3", $arrayPath[2]);


        return $query->execute();
    }
}
