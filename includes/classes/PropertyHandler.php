<?php
class PropertyHandler
{
  private $property;

  public function __construct($property)
  {
    $this->property = $property;
  }

  public function view()
  {

    $imgsrc1 = $this->property->getFilePath1();
    $imgsrc2 = $this->property->getFilePath2();
    $imgsrc3 = $this->property->getFilePath3();
    if (!isset($imgsrc1)) {
      $imgsrc1 = 'img/noimage.PNG';
    }
    if (!isset($imgsrc2)) {
      $imgsrc2 = 'img/noimage.PNG';
    }
    if (!isset($imgsrc3)) {
      $imgsrc3 = 'img/noimage.PNG';
    }

    $title = $this->property->getTitle();
    $uploadedBy = "Publier par " . $this->property->getUploadedBy();
    $uploadDate = "Published Date: " . $this->property->getUploadDate();
    $views = "Nombre de vues: " . $this->property->getViews();
    $description = htmlspecialchars_decode($this->property->getDescription());
    $takealotLink = $this->property->getTakealotLink();
    $type = "Type: " . $this->property->getType();
    $price = "Price: R" . $this->property->getPrice();
    $reducedprice = "Now: R" . $this->property->getReducedPrice();
    $deleteUrl = $this->property->getId();
    $num = $this->property->getUserNumber();

    // start this for cath
    if ((preg_match('#0817477817#', $num) === 1 || preg_match('#817477817#', $num) === 1 || preg_match('#817477817#', $num) === 1)) {
      $num2 = "+27817477817";
      $num = "+27817477817";
    }
    // end cath

    // start this for gloria
    if ((preg_match('#0843143040#', $num) === 1)) {
      $num2 = "+27843143040";
      $num = "+27843143040";
    }
    // end gloria

    // start this for dom
    if (preg_match('#0640490820#', $num) === 1) {
      $num2 = "+27640490820";
      $num = "+27640490820";
    }
    // end dom

    if (preg_match('#^0#', $num) === 1) {
      $num1 = substr($num, 1);
      $num2 = "+243" . $num1;
    };

    if ((preg_match('#^0#', $num) !== 1 && preg_match('#243#', $num) === 1)) {
      $num2 = "+243" . $num;
    }

    // $contactNumber = "<a href='$takealotLink' class='btn btn-primary animated fadeInUp' target='_blank' >Buy It With Takealot</a>" . "<a class='btn btn-info animated fadeInUp' href='tel:$num'>" . 'Call: ' . $num . "</a>" . "<a href='https://wa.me/$num2/?text=Hi, I am interrested in $title.' class='float-right'>" . "<img src='img/whatsappicon48.png' alt='whatsapp'></img>" . "</a>";

    $arrayImage = array();
    array_push($arrayImage, $imgsrc1, $imgsrc2, $imgsrc3);
    foreach ($arrayImage as $imgsrc) {
      # code...
    }

    if (isset($imgsrc1)) {
      return "

          <h5 class='card-title'>$title</h5>
        <div class='card'>
        <div class = 'text-center'>
        <img src='$imgsrc1' class='card-img-top pb-3' alt='image' style='width: 1000;'>
        </div>
        <div claa='card-body'>
          <p class='card-text'>$description</p>
          <p class='pt-5'>$uploadDate</p>
        </div>
        </div>

   ";
    }
  }

  public function edit()
  {

    $imgsrc1 = $this->property->getFilePath1();
    $imgsrc2 = $this->property->getFilePath2();
    $imgsrc3 = $this->property->getFilePath3();
    if (!isset($imgsrc1)) {
      $imgsrc1 = 'img/noimage.PNG';
    }
    if (!isset($imgsrc2)) {
      $imgsrc2 = 'img/noimage.PNG';
    }
    if (!isset($imgsrc3)) {
      $imgsrc3 = 'img/noimage.PNG';
    }
    $title = $this->property->getTitle();

    $takaalotLink = $this->property->getTakealotLink();
    $buyOnTake = "<a href='$takaalotLink' class='btn btn-primary animated fadeInUp' target='_blank' >See On Takealot</a>";
    $uploadedBy = "Publier par " . $this->property->getUploadedBy();
    $uploadDate = "Published Date: " . $this->property->getUploadDate();
    $views = "Nombre de vues: " . $this->property->getViews();
    $description = htmlspecialchars_decode($this->property->getDescription());
    $takealotLink = $this->property->getTakealotLink();
    $type = "Type: " . $this->property->getType();
    $price = "Price: R" . $this->property->getPrice();
    $reducedprice = "Now: R" . $this->property->getReducedPrice();
    $deleteUrl = $this->property->getId();
    $num = $this->property->getUserNumber();

    if (isset($imgsrc1)) {
      return "

        <button type='button' class='btn btn-primary float-right' data-toggle='modal' data-target='#exampleModal'>
        Delete
         </button>
         <h5 class='card-title'>$title</h5>
         <div class='card'>
         <div class = 'text-center'>
         <img src='$imgsrc1' class='card-img-top pb-3' alt='image' style='width: ;'>
         </div>
         <div claa='card-body'>
           <p class='card-text'>$description</p>
           <p class='pt-5'>$uploadDate</p>
         </div>
         </div>


        <!-- Modal -->
        <div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
        <div class='modal-dialog'>
          <div class='modal-content'>
            <div class='modal-header'>
              <h5 class='modal-title' id='exampleModalLabel'>Delete this article?</h5>
            </div>
            <div class='modal-body'>
            If you delete it, you will need to publish a new one.
            </div>
            <div class='modal-footer'>
        
              <a href='deletearticle.php?id=$deleteUrl' class='btn btn-primary'>Yes Delete</a>
              <button type='button' class='btn btn-secondary' data-dismiss='modal'>No</button>
            </div>
          </div>
        </div>
        </div>";
    }
  }
}
