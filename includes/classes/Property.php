<?php

class Property
{
    private $con, $sqlData, $userLoggedInObj;

    public function __construct($con, $input, $userLoggedInObj)
    {
        $this->con = $con;
        $this->userLoggedInObj = $userLoggedInObj;


        if (is_array($input)) {
            $this->sqlData = $input;
        } else {

            $query = $this->con->prepare("
            SELECT `products`.`id`,
            `products`.`userId`,
            `products`.`type`,
            `products`.`price`,
            `products`.`reduced_price`,
            `products`.`uploadedBy`,
            `products`.`title`,
            `products`.`description`,
            `products`.`takealotLink`,
            `products`.`filePath1`,
            `products`.`filePath2`,
            `products`.`filePath3`,
            `products`.`category`,
            `products`.`uploadDate`,
            `products`.`views`,
            `products`.`category_id`,
            `categories`.`value`,
            `categories`.`name`,
            `users`.phone_number
        FROM `products`
         LEFT JOIN categories ON categories.id = products.category_id 
         LEFT JOIN users ON users.id = products.userId 
         WHERE products.id = :id ");
            $query->bindParam(":id", $input);
            $query->execute();

            $this->sqlData = $query->fetch(PDO::FETCH_ASSOC);

        }
    }


    public function create($property)
    {

        $url = "view.php?id=" . $property->getId();
        $imgsrc1 = $property->getFilePath1();
        $imgsrc2 = $property->getFilePath2();
        $imgsrc3 = $property->getFilePath3();

        if(!isset($imgsrc1)){
          $imgsrc1 = 'img/products/noimage.PNG';
        }
        if(!isset($imgsrc2)){
          $imgsrc2 = 'img/products/noimage.PNG';
        }
        if(!isset($imgsrc3)){
          $imgsrc3 = 'img/products/noimage.PNG';
        }

        $title = $property->getTitle();
        $description = $property->getDescription();
        $takaalotLink = $property->getTakealotLink();
        $buyOnTake = "<a href='$takaalotLink' class='btn btn-primary animated fadeInUp' target='_blank' >Buy It With Takealot</a>";
        $uploadedBy = "Publier par " . $property->getUploadedBy();
        $uploadDate = "Date: " . $property->getUploadDate();
        $views = "Nombre de vues: " . $property->getViews();
        $type = "Category: " . $property->getType();
        $price = "Price: R" . $property->getPrice();
        $reducedprice = "Now: R" . $property->getReducedPrice();

        // $ville = "Ville: " . $property->getVille();

        if (isset($imgsrc1)) {
          return "
          <hr>
          <div class='card mb-3' style='max-width: ;'>
          <a href='$url'>
          <div class='row no-gutters'>
            <div class='col-md-2'>
              <img src='$imgsrc1' class='card-img' alt='image'>
            </div>
            <div class='col-md-8'>
              <div class='card-body'>
                <h5 class='card-title'>$title</h5>
                <p class='card-text'>Read More ...</p>
                <p class='card-text'><small class='text-muted'>$uploadDate</small></p>
              </div>
            </div>
          </div>
          </a>
        </div>";
        }
    }

    public function mycreate($property)
    {
        $url = "edit.php?id=" . $property->getId();
        $imgsrc1 = $property->getFilePath1();
        $imgsrc2 = $property->getFilePath2();
        $imgsrc3 = $property->getFilePath3();

        if(!isset($imgsrc1)){
          $imgsrc1 = 'img/products/noimage.PNG';
        }
        if(!isset($imgsrc2)){
          $imgsrc2 = 'img/products/noimage.PNG';
        }
        if(!isset($imgsrc3)){
          $imgsrc3 = 'img/products/noimage.PNG';
        }

        $takaalotLink = $property->getTakealotLink();
        $buyOnTake = "<a href='$takaalotLink' class='btn btn-primary animated fadeInUp' target='_blank' >Buy It With Takealot</a>";
        $title = $property->getTitle();
        $description = $property->getDescription();
        $uploadedBy = "Publier par " . $property->getUploadedBy();
        $uploadDate = "Date: " . $property->getUploadDate();
        $views = "Nombre de vues: " . $property->getViews();
        $type = "Category: " . $property->getType();
        $price = "Price: R" . $property->getPrice();
        $reducedprice = "Now: R" . $property->getReducedPrice();
        // $ville = "Ville: " . $property->getVille();
        if (isset($imgsrc1)) {
            return "
            <hr>
            <div class='card mb-3' style='max-width: ;'>
            <a href='$url'>
            <div class='row no-gutters'>
              <div class='col-md-2'>
                <img src='$imgsrc1' class='card-img' alt='image'>
              </div>
              <div class='col-md-8'>
                <div class='card-body'>
                  <h5 class='card-title'>$title</h5>
                  <p class='card-text'>Read More ...</p>
                  <p class='card-text'><small class='text-muted'>$uploadDate</small></p>
                </div>
              </div>
            </div>
            </a>
          </div>";
        } 
    }

    public function generateItemsFromproperties($properties)
    {
        $elementsHtml = "";
        foreach ($properties as $property) {
            $elementsHtml .= $this->create($property);
        }

        return $elementsHtml;
    }

    public function generateItemsFromMyproperties($properties)
    {

        $elementsHtml = "";

        foreach ($properties as $property) {
            $elementsHtml .= $this->mycreate($property);
        }

        return $elementsHtml;
    }

    public function getId()
    {
        return $this->sqlData["id"];
    }
    public function getUserNumber()
    {
        return $this->sqlData["phone_number"];
    }
    public function getUploadedBy()
    {
        return $this->sqlData["uploadedBy"];
    }

    public function getTitle()
    {
        return $this->sqlData["title"];
    }
    public function getDescription()
    {
        return $this->sqlData["description"];
    }
    public function getTakealotLink()
    {
        return $this->sqlData["takealotLink"];
    }
    public function getFilePath1()
    {
        return $this->sqlData["filePath1"];
    }

    public function getFilePath2()
    {
        return $this->sqlData["filePath2"];
    }

    public function getFilePath3()
    {
        return $this->sqlData["filePath3"];
    }
    public function getUploadDate()
    {
        return $this->sqlData["uploadDate"];
    }
    public function getPrice()
    {
        $ret = "Contacter pour le prix";
        if ($this->sqlData["price"] != null) {
            return $this->sqlData["price"];
        } else {
            return $ret;
        }
    }

    public function getReducedPrice()
    {
        $ret = "No in stock";
        if ($this->sqlData["reduced_price"] != null) {
            return $this->sqlData["reduced_price"];
        } else {
            return $ret;
        }
    }

    public function getVille()
    {
        return $this->sqlData["name_ville"];
    }
    public function getType()
    {
        return $this->sqlData["name"];
    }
    public function getViews()
    {
        return $this->sqlData["views"];
    }

    public function getUserId()
    {
        return $this->sqlData["userId"];
    }



    public function incrementViews()
    {
        $query = $this->con->prepare("UPDATE products SET views=views+1 WHERE id=:id");
        $query->bindParam(":id", $propertyId);

        $propertyId = $this->getId();
        $query->execute();

        $this->sqlData["views"] = $this->sqlData["views"] + 1;
    }


    public function deleteArticle()
    {
        $userId = $this->getUserId();
        if (isset($userId)) {
            $propertyId = $this->getId();
            $query = $this->con->prepare("SELECT id FROM products WHERE id=$propertyId AND userId = $userId;");
            $propertyIdToDelete = $query->execute();
            if (isset($propertyIdToDelete)) {
                $query1 = $this->con->prepare("DELETE FROM products WHERE id=$propertyId;");
                $articleDelteted = $query1->execute();
                return true;
            } else {
                echo "  <div class='row'>
                <div class='col s12'>
                  <div class='card white darken-1'>
                    <div class='card-content green-text'>
                      <h5 class='center-align'><span class='card-title '><i class='material-icons Large'>check_circle</i></span></h5><br>
                      <h5 class='center-align'>Vous n'êtes pas autorisés à éffectué cette action.</h5>
                    </div>
                    <div class='card-action'>
                      <a href='articles.php' class='blue-text'>Cliquez Ici pour retourner aux articles.</a>
                    </div>
                  </div>
                </div>
                </div>
                
                <div class='progress'>
                <div class='indeterminate'></div>
                </div>";
                return false;
            }
            return true;
        } else {
            echo "  <div class='row'>
            <div class='col s12'>
              <div class='card white darken-1'>
                <div class='card-content green-text'>
                  <h5 class='center-align'><span class='card-title '><i class='material-icons Large'>check_circle</i></span></h5><br>
                  <h5 class='center-align'>Vous n'êtes pas autorisés à éffectué cette action.</h5>
                </div>
                <div class='card-action'>
                  <a href='articles.php' class='blue-text'>Cliquez Ici pour retourner aux articles.</a>
                </div>
              </div>
            </div>
            </div>
            
            <div class='progress'>
            <div class='indeterminate'></div>
            </div>";
            return false;
        }
    }
}
