<?php
class ImageUploadData {

    public $title, $description, $file1, $file2, $file3, $userId, $uploadedBy;

    public function __construct($title, $description, $file1, $file2, $file3, $userId, $uploadedBy){

        $this->title = $title;
        $this->description = $description;
        $this->file1 = $file1;
        $this->file2 = $file2;
        $this->file3 = $file3;
        $this->userId = $userId;
        $this->uploadedBy = $uploadedBy;
        

    }

}

?>