<?php
require_once("includes/header.php");
require_once("includes/classes/PropertyHandler.php");



if (!isset($_GET["id"])) {
  echo "oops no products";
  exit();
}

$property = new Property($con, $_GET["id"], null);
$property->incrementViews();

?>


<div class="container">
<div class="row pt-5 mt-5">
          <div class="col-md-12">
            <h5 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Ecliips Blog Details</h5>
          </div>
        </div>
<div class="row">
  <div class="col-sm-12">
<div class="card" >
  <div class="card-body">
  <?php
  $PropertyHandler = new PropertyHandler($property);
  echo $PropertyHandler->view();


  ?>
</div>
</div>
</div>
</div>
</div>
<?php require_once("includes/deleteModal.php"); ?>


<?php require_once("includes/footer.php"); ?>