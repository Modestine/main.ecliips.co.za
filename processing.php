<?php 
require_once("includes/header.php"); 
require_once("includes/classes/ImageUploadData.php");
require_once("includes/classes/ImageProcessor.php"); 



if(!isset($_POST["uploadButton"])){

    echo "   <div class='card blue-grey darken-1'>
    <div class='card-content white-text'>
      <span class='card-title'>Oh la la, Une erreur se produit!</span>
      <p class='>Ceci apparait lorsque vous avez entré une mauvaise addresse ou le lien vers cette addresse est endommagé.</p>
      <hr>
      <p>Notre équipe est au courant et y travaille. Merci pour avoir choisi Ujisha. </p>
    </div>
    <div class='card-action'>
      <a href='product.php'>Cliquez ici pour aller à la page d'acceuil.</a>
 
    </div>
  </div>";
    exit();

};
 

// create file upload data

if(isset($_FILES["file1"])){
    $file1 = $_FILES["file1"];
}else{
    $file1 = "null";
}

if(isset($_FILES["file2"])){
    $file2 = $_FILES["file2"];
}else{
    $file2 = "null";
}

if(isset($_FILES["file3"])){
    $file3 = $_FILES["file3"];
}else{
    $file3 = "null";
}

$ImageUploadData = new ImageUploadData(
$_POST["title"], 
htmlspecialchars($_POST["description"]),
 $file1, 
 $file2, 
 $file3,
 $userLoggedInObj->getUserId(),
 $userLoggedInObj->getFullName()

);
// process image data (upload)

$ImageProcessor = new ImageProcessor($con);
$wasSuccessful = $ImageProcessor->upload($ImageUploadData);

// check if upload was successful

if($wasSuccessful){
    header("Location: admin.php");
}
