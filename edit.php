<?php
require_once("includes/header.php");
require_once("includes/classes/PropertyHandler.php");



if (!isset($_GET["id"])) {
    echo "Oops product does not exist";
    exit();
}

?>

<div class="container">
<div class="row pt-5 mt-5">
          <div class="col-md-12">
            <h5 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Edit</h5>
          </div>
        </div>

<div class="card" >
  <div class="card-body">
  <?php
       $property = new Property($con, $_GET["id"], null);
  $PropertyHandler = new PropertyHandler($property);
  echo $PropertyHandler->edit();


  ?>
</div>
</div>
</div>
<?php require_once("includes/deleteModal.php"); ?>


<?php require_once("includes/footer.php"); ?>