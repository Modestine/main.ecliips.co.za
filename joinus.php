<?php
require_once("includes/headerindex.php");
?>
<!-- sliders -->
<div id="sliders">
  <div class="full-width">
    <!-- light slider -->
    <div id="light-slider" class="carousel slide">
      <div id="carousel-area">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <!-- <li data-target="#carousel-slider" data-slide-to="0" class="active"></li> -->
            <!-- <li data-target="#carousel-slider" data-slide-to="1"></li> -->
            <!--  <li data-target="#carousel-slider" data-slide-to="2"></li> -->
          </ol>
          <!-- <div class="carousel-inner" role="listbox"> -->
          <div>
            <div class="carousel-item active ">
              <img src="img/joinus/joinimg.png" alt="">
              <div class="carousel-caption pr-4">
                <!-- <a href="#specials" class="btn btn-lg btn-border animated fadeInUp">JOIN US</a> -->

              </div>
            </div>

          </div>
          <!-- <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
          </a>
          <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
            <i class="fa fa-chevron-right"></i>
          </a> -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End sliders -->

</header>

<!-- About Section Start -->
<div id="" class="section-padding ecliipsBB">
  <div class="container p-5">
    <div class="row p-5">
      <div class="col-md-12">
        <h6 class="section-title wow fadeInDown animated  text-white " data-wow-delay="0.3s">SELECT YOUR AREA OF APPLICATION</h6>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <a href="apply.php">
            <img src="img/joinus/driver.png" alt="" class="zoom">
          </a>
          <!-- <h5 class="pt-4"><a class="textStore" href="#">Food</a></h5> -->
        </div>

      </div>

      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <a href="apply.php">
            <img src="img/joinus/influencer.png" alt="" class="zoom">
          </a>
        </div>
        <!-- <h5 class="pt-4"><a class="textStore"  href="#">Health Care</a></h5> -->
      </div>

      <div class="col-lg-4 col-md-6 col-xs-12 text-center">
        <div class="">
          <a href="apply.php">
            <img src="img/joinus/supplier.png" alt="" class="zoom">
          </a>
        </div>
        <!-- <h5 class="pt-4"><a class="textStore"  href="#">Afro Fusion Boutique</a></h5> -->
      </div>

    </div>
  </div>

</div>
<hr style="height:2px;border-width:0;color:gray;background-color:gray">

<?php
require_once("includes/footer.php");
?>