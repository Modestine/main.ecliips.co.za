<?php
  require_once 'EmailSender.php';
class EmailManager
{
    // public function SendEmailTemplate(string $to, int $messagetemplateid, $data)
    // {
    //     $messagetemplate = $this->_GetMessageTemplate($messagetemplateid);
    //     if($messagetemplate == null) return false;
    //     $emailTemplate = $this->_GetEmailTemplate('plugins/email/emailtemplate.html');
    //     if ($emailTemplate !== false){
    //         $success = $this->SendEmailWithTemplate($to,$emailTemplate, $messagetemplate,$data);
    //         return $success;
    //     }
    //     return false;
    // }

    public function SendEmailWithTemplate(string $to, string $emailTemplate, $messagetemplate, $data)
    {
        $body = $this->_PlaceholderReplace($messagetemplate->body, $data);
        $subject = $this->_PlaceholderReplace($messagetemplate->subject, $data);

        $data->body = $body;
        $data->subject = $subject;

        $body = $this->_PlaceholderReplace($emailTemplate, $data);
    
      
        $emailSender = new EmailSender();
        $success = $emailSender->SendEmail($to, $subject, $body);

        return $success;
    }

    public function SendEmail(string $to, string $subject, string $body)
    {
        $emailSender = new EmailSender();
        return $emailSender->SendEmail($to, $subject, $body);
    }

    // private function _GetMessageTemplate($event)
    // {
    //     $qry = "SELECT * FROM `emailmessage_template` WHERE `emailmessage_event_type` = $event;";
    //     $config = new Config();
    //     $db = new DataService($config->dbConnection);
    //     $data = $db->ExecStatement($qry);

    //      if(sizeof($data)  == 1){
    //         return (object)$data[0];
    //     }
    //     return null;
    // }

    private function _GetEmailTemplate($emailtemplatefile){
        return file_get_contents($emailtemplatefile);
    }

    private function _PlaceholderReplace($message,$data){
        foreach($data as $key => $value){
            $field = "{".$key."}";
            $message = str_replace($field,$value,$message);
        }
        return $message;
    }
}
