<?php
require_once("includes/header.php");
?>
<!-- Services Section Start -->
<div id="" class="section-padding">
  <div class="container">
  <div class="row pt-5">
      <div class="col-md-12">
        <h4 class="section-title wow fadeInDown animated" data-wow-delay="0.3s">Our Stores</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img1.png" alt="">
          <h5><a href="#">Store 1</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img2.png" alt="">
          <h5><a href="#">Store 2</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img3.png" alt="">
          <h5><a href="#">Store 3</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img4.png" alt="">
          <h5><a href="#">Store 4</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
    </div>
  </div>



  <div class="container pt-5">

    <div class="row">
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img1.png" alt="">
          <h5><a href="#">Store 1</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img2.png" alt="">
          <h5><a href="#">Store 2</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img3.png" alt="">
          <h5><a href="#">Store 3</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <div class="about block text-center">
          <img src="img/about/img4.png" alt="">
          <h5><a href="#">Store 4</a></h5>
          <p>Quisque sit amet libero purus. Nulla a dignissim quam. In hac habitasse platea dictumst.</p>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- Services Section End -->

<?php
require_once("includes/footer.php");
?>